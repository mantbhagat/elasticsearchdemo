﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearchDemo.Models
{
    public class Locations 
    {
        [Ignore]
        public string LocationType { get; set; }
        public List<int> AllowedPartners { get; set; }
        [Ignore]
        public int PartnerId { get; set; }
        public string BrandName { get; set; }
        public string BrandType { get; set; }
        public bool? HasShowCase { get; set; }
        public int Type { get; set; }
        public GeoLocation Location { get; set; }
        public int? TotalLikedImages { get; set; }
        public string MarketStateName { get; set; }
        public string Market { get; set; }
        public int MarketId { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public bool hasPark { get; set; }
        public bool hasPool { get; set; }
        public bool hasSport { get; set; }
        public CompletionField Suggest { get; set; }
        public int BdxId { get; set; }
        public string Id { get; set; }
        public string MarketState { get; set; }
        public int? TotalClassifiedImages { get; set; }
    }
}
