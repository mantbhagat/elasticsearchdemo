﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearchDemo.Models
{
    public class SearchImages
    {
        public int? MasterBrLoc { get; set; }
        public DateTime? MoveInDate { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string PlanViewerPath { get; set; }
        public int ListingId { get; set; }
        public DateTime? SoldDate { get; set; }
        public float? SoldPrice { get; set; }
        public int? SpecId { get; set; }
        public string LogoUrlSmall { get; set; }
        public string SpecSalesStatus { get; set; }
        public string SpecNumber { get; set; }
        public float? Stories { get; set; }
        public string Thumb { get; set; }
        public string Thumb2 { get; set; }
        [Obsolete]
        public string Type { get; set; }
        public string VirtualTourPath { get; set; }
        public string HomeType { get; set; }
        public string ListingNumber { get; set; }
        public string SpecificationUrl { get; set; }
        public string SpecType { get; set; }
        public string PlanUrl { get; set; }
        public string LogoUrlMed { get; set; }
        public int? LivAreas { get; set; }
        public int? HomeId { get; set; }
        public string AddressPlan { get; set; }
        public int CommId { get; set; }
        public string CommunityName { get; set; }
        public float? Distance { get; set; }
        public string ElevPath { get; set; }
        public string FloorPlanPath { get; set; }
        public int? HalfBa { get; set; }
        public bool? HasVideo { get; set; }
        [GeoPoint]
        public GeoLocation Location { get; set; }
        public string HomeStatus { get; set; }
        public string IntImage { get; set; }
        public int? ImageCount { get; set; }
        [Ignore]
        public bool IsBasic { get; set; }
        public bool IsHotHome { get; set; }
        public bool IsLandExcluded { get; set; }
        public bool IsLuxury { get; set; }
        public bool IsQmi { get; set; }
        public bool IsSold { get; set; }
        public bool IsSpec { get; set; }
        public int MarketId { get; set; }
        public bool hasPark { get; set; }
        public bool hasPool { get; set; }
        public bool hasSport { get; set; }
        public string MarketName { get; set; }
        public bool IsBasicListing { get; set; }
    }
}
