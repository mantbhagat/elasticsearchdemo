﻿using ElasticSearchDemo.Models;
using Microsoft.AspNetCore.Mvc;
using Nest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ElasticSearchDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IElasticClient _elasticClient;
        public UsersController(IElasticClient elasticClient)
        {
            this._elasticClient = elasticClient;
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public async Task<User> Get(string id)
        {
            var response = await this._elasticClient.SearchAsync<User>(s =>
            s.Index("users")
            .Query(q => q.Match(m => m.Field(f => f.Name).Query(id))
            ));
            return response?.Documents?.FirstOrDefault();
            // Second way to get data by GetAsync
            //  var responseById = await this._elasticClient.GetAsync<User>(new DocumentPath<User>(new Id(id)), x => x.Index("users"));
            //  var data = responseById?.Source;
            // return new User { };
        }

        // POST api/<UsersController>
        [HttpPost]
        public async Task<string> Post([FromBody] User value)
        {
            var response = await this._elasticClient.IndexAsync<User>(value, v => v.Index("users"));
            return response.Id;
        }

        [HttpGet("matchingscore/{marketId}")]
        public async Task<List<SearchImages>> MatchingScore(int marketId)
        {
            //            var response = await this._elasticClient.SearchAsync<Locations>(v =>
            //v.Index("locations")
            //.Query(q => q.Bool(b => b.Must(m => m.Match(ma => ma.Field(f => f.MarketId).Query(marketId.ToString())))));

            var response = await this._elasticClient.SearchAsync<SearchImages>(v =>
            v.Index("searchimages")
            .Query(q => q

            .FunctionScore(f => f
            .Query(fq => fq
            .Bool(b => b
            .Must(
                m1 => m1.Match(ma => ma.Field(maf1 => maf1.MarketName).Query("dallas")),
                m2 => m2.Match(ma1 => ma1.Field(maf => maf.MarketId).Query(marketId.ToString()))
                )))

            .Functions(fsf => fsf.ScriptScore(fs =>
              fs
              .Script(fss => fss.Source("doc['baHi'].value >= params.baths ? 1 : (params.baths - doc['baHi'].value <= 1 ? 0.5 : 0)")
              .Params(fsp => fsp.Add("baths", 3))).Weight(5)

            )

            .ScriptScore(fs =>
              fs
              .Script(fss => fss.Source("doc['brHi'].value >= params.bed ? 1 : (params.bed - doc['brHi'].value <= 1 ? 0.5 : 0)")
              .Params(fsp => fsp.Add("bed", 3))).Weight(5)
            )

            .ScriptScore(fs =>
              fs.Script(fss => fss.Source("doc['sftHi'].value >= params.sqft ? 1 : (params.sqft - doc['sftHi'].value <= 500 ? 0.5 : 0)")
.Params(fsp => fsp.Add("sqft", 2500))).Weight(5)
            )

            .ScriptScore(fs =>
              fs
               .Script(fss => fss.Source("doc['stories'].value == params.story ? 1 : 0")
              .Params(fsp => fsp.Add("story", 2))).Weight(5)
            )

             .ScriptScore(sc => sc
                  .Script(scc => scc
                     .Source("doc['marketId'].value == params.marketId ? 1 : 0.5")
                     .Params(p => p.Add("marketId", marketId)))
                  .Weight(20)
            )

            //.FieldValueFactor(fvf =>
            //fvf.Filter(f => f.Match(mf => mf.Field(b => b.MarketId == marketId)))
            //.Weight(20))

            //.FieldValueFactor(fvf => fvf
            //.Filter(f => f.Match(m => m.Field(mf => mf.hasPark == true)))
            //.Filter(f => f.Match(m => m.Field(mf => mf.hasPool == true)))
            //.Filter(f => f.Match(m => m.Field(mf => mf.hasSport == true)))
            //.Weight(15))

            .ScriptScore(sc => sc
            .Script(scc => scc
            .Source("doc['garageHi'].value >= params.garage ? 1 : (params.garage - doc['garageHi'].value <= 1 ? 0.5 : 0)")
            .Params(p => p.Add("garage", 2)))
            .Weight(5)
            )

            .ScriptScore(sc => sc
            .Script(scc => scc
            .Source("doc['prHi'].value > params.budgetHigh ? 0 : ((doc['prHi'].value >= params.budgetLow || doc['prHi'].value <= params.budgetHigh) ? 1: 0.5)")
            .Params(p => p.Add("budgetLow", 100000).Add("budgetHigh", 300000)))
            .Weight(20)
            )

            )
            .MaxBoost(100)
            .ScoreMode(FunctionScoreMode.Sum)
            .BoostMode(FunctionBoostMode.Replace)
            ))
            .Size(22)
            .Source(s => s.Includes(b => b
            .Field(b => b.PlanId)
            .Field(f => f.PlanName)
            .Field(v => v.CommunityName)
            .Field(v => v.AddressPlan)
            .Field(v => v.IsSpec)
            .Field(v => v.MarketId)
            .Field(v => v.MarketName)
            ))

            );

            return response.Documents.ToList();
        }


        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
